package com.mybatis.enhance.store.entities;

import java.util.List;

public class TableEntity {
	
	private String name;
	
	private String comment;
	
	private List<Object> fieldList;
	
	public TableEntity(){}
	
	public TableEntity(String name, String comment, List<Object> fieldList) {
		super();
		this.name = name;
		this.comment = comment;
		this.fieldList = fieldList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Object> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<Object> fieldList) {
		this.fieldList = fieldList;
	}
	
	

}
